---
title: "Anatomy of Hungarian aspectual particles"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- Aniko Csirmaz
- admin


# Author notes (optional)
author_notes:
 - "Equal contribution"
 - "Equal contribution"

date: "2020-04-08T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: '*Approaches to Hungarian: Volume 16: Papers from the 2017 Budapest Conference*'
publication_short: '*Approaches to Hungarian 16*'

abstract: The paper explores a common core of meaning that various aspectual adverbials in Hungarian (including *megint*, *ismét* ‘again’, *még* ‘still’) share. It is proposed that there is a general definition for various aspectual elements related to times and events. We suggest that some components of those elements, including the scalar argument and the focus set, can vary – and this results in the different specific meanings. We assume that meaning is compositional and also address the complex form *mégis* ‘(concessive) still’ and other elements that appear to be synonymous with *mégis*.

# Summary. An optional shortened abstract.
summary: Proposal for a common underlying semantic component of various aspectual adverbials in Hungarian.

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
<!-- links: -->
<!-- - name: website -->
<!--   url: https://benjamins.com/catalog/atoh.16.02csi -->

url_pdf: 'https://benjamins.com/catalog/atoh.16.02csi'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
