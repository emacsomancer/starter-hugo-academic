---
title: "Hwæt! LOL! Common formulaic functions in 'Beowulf' and blogs"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- Matt Garley
- admin
- Marina Terkourafi


# Author notes (optional)
author_notes:
- "Equal contribution"
- "Equal contribution"

date: "2012-01-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: "*Chicago Linguistic Society* 2012"
publication_short: "*CLS* 2012"

abstract: "An examination of the discourse-structuring functions that formulae perform in two genres which exist in written format as texts, but maintain close links to oral forms, namely Old English (OE) verse, specifically the epic poem Beowulf, and weblogs. We identify five important functions of formulae found in common across OE verse and blogs, classifying these functions as discourse-structuring functions, filler functions, epithetic functions, gnomic functions, and tonic functions. In addition, a sixth type of formulaic function necessarily tied to the written medium, the acronymic function, is identified in both genres."

# Summary. An optional shortened abstract.
summary: "An examination of common discourse-structuring functions in the Old English epic 'Beowulf' and modern blogs."

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)

url_pdf: 'publications/garley_slade_terkourafi-2012preprint-formula_in_beowulf_blogs-cls45.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
