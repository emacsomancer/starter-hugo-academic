---
title: "A short history of English epistemic indefinites"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2015-08-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "*Epistemic Indefinites: Exploring Modality Beyond the Verbal Domain*, edited by Luis Alonso-Ovalle & Paula Menéndez-Benito, Oxford: Oxford University Press: 100-113"
publication_short: "*Epistemic indefinites*,  L. Alonso-Ovalle & P. Menéndez-Benito (eds.), OUP"

abstract: "In modern English there are two primary epistemic indefinites, *some NP* and *some NP or other* which bear distinct pragmatic properties, differing from one another both with respect to signalling how the speaker is (un)able to identify the individual in question and with respect to their relation to specificity. This chapter provides a preliminary examination of the synchronic and diachronic properties of these two English epistemic indefinites, along with other constructions associated with epistemicity, such as Old English postnominal *nathw-*. Texts from the Old English, Middle English, Early Modern English, and Modern English periods are examined in order to determine the development of the epistemic and specific uses of various constructions."

# Summary. An optional shortened abstract.
summary: "This study examines the history of English epistemic indefinites like *some man* and *some man or other*, examining earlier structures in Middle and Old English."

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
links:
- name: Book-website
  url: http://www.oxfordscholarship.com/view/10.1093/acprof:oso/9780199665297.001.0001/acprof-9780199665297-chapter-5

url_pdf: 'publications/slade-english-hist-eis-x1.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
