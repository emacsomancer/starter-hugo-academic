---
title: "Question Particles and Relative Clauses in the History of Sinhala, with Comparison to Early and Modern Dravidian"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2013-01-02T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "*Grammatica et verba/Glamor and verve: Studies in South Asian, historical, and Indo-European linguistics in honor of Hans Henrich Hock on the occasion of his seventy-fifth birthday*, edited by Shu-Fen Chen & Benjamin Slade, Ann Arbor, MI: Beech Stave Press: 245–268."
publication_short: "*Glamor and verve*, S.-F. Chen & B. Slade (eds.), Beech Stave"

abstract: "This paper examines the evolution of two separate but interrelated aspects of the grammar of Sinhala: the distribution of the Question-particle *da* and the formation of relatives clauses in the history of Sinhala. I also examine the structure of relative clauses and the distribution of Question-particles in genetically-unrelated but geographically-proximate Dravidian. The possible role of language contact in the evolution of certain Sinhala grammatical structures—including the employment of the particle *da*—which have no parallels in other Indo-Aryan languages—is also considered."

# Summary. An optional shortened abstract.
summary: "An examination of relative clauses in the history of Sinhala, with comparison to Dravidian."

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
links:
- name: Book-website
  url: http://www.beechstave.com/gev.html

url_pdf: 'publications/slade-sinhala-relative-clauses-q-particles-preprint.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
