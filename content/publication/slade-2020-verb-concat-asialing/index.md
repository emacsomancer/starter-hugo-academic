---
title: "Verb Concatenation in Asian Linguistics"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2020-08-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "*Oxford Research Encyclopedia of Linguistics*"


abstract: "Across a large part of Asia are found a variety of verb-verb collocations, a prominent subset of which involves collocations typically displaying completive or resultative semantics. Such collocations are found in Indo-Aryan and Dravidian languages of South Asia, Turkic and Iranian languages of Central Asia, and in Chinese languages. In South and Central Asian languages, verb-verb collocations usually involve some added aspectual/Aktionsart element of meaning, frequently (though not exclusively) indicating completion of an event and sometimes involving speaker evaluation of the event (e.g., surprise, regret). Thus Hindi *Rām-ne kitāb paṛh diyā*, literally “John read-gave the book,” with the sense “John read the book out.” In Chinese languages, many verb-verb collocations involve a resultative sense, similar to English “Kim ran herself/her shoes ragged.” However, earlier Chinese verb-verb collocations were agent-oriented, for example, *She-sha Ling Gong* “(Someone) shot and killed Duke Ling,” where she is “shoot” and sha is “kill.” In Indo-Aryan, Dravidian, and Central Asian languages, we find verb-verb collocations that evolve from idiomaticization and grammaticalization of constructions involving converbs, for example, a collocation meaning “he, having eaten food, left” acquires the meaning “he ate food (completely).” Similarly, the Chinese verb-verb resultatives derive from earlier verb-verb “co-ordinate” constructions (originally with an overt morpheme *er*: *ji er sha zhi* “struck and killed him”), which functionally is similar to the role of converbs in South and Central Asian languages. While these Asian verb-verb collocations are strikingly similar in broad strokes, there are significant differences in the lexical, semantic, and morphosyntactic properties of these constructions in different languages. This is true even in closely related languages in the same language family, such as in Hindi and Nepali. The historical relation between verb-verb collocations in different Asian languages is unclear. Even in geographically proximate language families such as Indo-Aryan and Dravidian, there is evidence of independent development of verb-verb collocations, with possible later convergence. Central Asian verb-verb collocations being very similar in morphosyntactic structure to South Asian verb-verb collocations, it is tempting to suppose that for these there is some contact-based cause, particularly since such collocations are much less prominent in Turkic and Iranian languages outside of Central Asia. The relation between South and Central Asian verb-verb collocations and Chinese verb-verb collocations is even more opaque, and there are greater linguistic differences here. In this connection, further study of verb-verb collocations in Asian languages geographically intermediate to Central and South Asia, including Thai, Vietnamese, and Burmese, is required."

# Summary. An optional shortened abstract.
summary: "Survey of a subset of verb-verb collocations in Asia (including South, Central, and East Asia), especially those involving completive/resultative semantics."

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
links:
- name: encyclopedia-website
  url: https://oxfordre.com/linguistics/view/10.1093/acrefore/9780199384655.001.0001/acrefore-9780199384655-e-302

url_pdf: 'https://ling.auf.net/lingbuzz/005505/current.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
