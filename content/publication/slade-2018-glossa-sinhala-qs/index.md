---
title: "History of focus-concord constructions and focus-associated particles in Sinhala, with comparison to Dravidian and Japanese"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2018-01-05T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Glossa: a journal of general linguistics* 3(1)"
publication_short: "*Glossa* 3(1)"


abstract: "This study traces the historical development of the focus concord construction of Sinhala from the language of the pre-second millennial graffiti on the Mirror Wall at Sigiriya to the modern colloquial language, with comparison to the historical development of focus concord constructions in the south Dravidian languages Malayalam and Tamil, as well as the focus concord (*kakari-musubi*) construction of Japanese. I argue that the Sinhala focus concord construction originated as one particular usage of impersonal verbal nominalisations in Old Sinhala, developed into a predicative clefting construction in Classical Sinhala, and in the modern colloquial language has become a phenomenon involving verb forms showing a sort of agreement with focussed elements."

# Summary. An optional shortened abstract.
summary: "This study traces the historical development of the focus concord construction of Sinhala with comparison to Dravidian & Japanese."

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: 'https://www.glossa-journal.org/article/4970/galley/12031/download/'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
