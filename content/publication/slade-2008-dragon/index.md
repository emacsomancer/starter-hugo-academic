---
title: "How (exactly) to slay a dragon in Indo-European? PIE *bheid- {h₃égʷhim, kʷṛ́mi-}"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2008-07-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Historische Sprachforschung* 121:3–53."
publication_short: "*Historische Sprachforschung* 121"

abstract: "In this paper I present evidence for a formula associated with the Indo-European dragon-slaying myth, PIE * *bheid-* {h₃égʷhim, kʷrmi-} 'split serpent/worm'. This formula is robustly attested in Vedic in the form *bhid- áhi-*, alongside the variant *vraśc- áhi-* 'tear/split serpent', with possible reflexes being found also in Iranian and Germanic. Though not as widely attested as PIE * *gʷhen- h₃égʷhi-* 'slay serpent' — a formula discussed in great detail by Watkins 1987, 1995 — * *bheid-* {h₃égʷhim, kʷrmi-} 'split serpent/worm' is semantically more specific, and therefore more distinctive, than * *gʷhen- h₃égʷhim*, thus lending additional support for Watkins' thesis that there exists a distintively Indo-European dragon-slaying myth."

# Summary. An optional shortened abstract.
summary: "Evidence for a new formula associated with the Indo-European dragon-slaying myth."

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
links:
- name: journal-website
  url: https://www.jstor.org/stable/i40077836

url_pdf: 'publications/Slade2008[2010]-How_(exactly)_to_slay_a_dragon-hs121(article_only).pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
