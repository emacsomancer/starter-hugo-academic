---
title: "How to rank constraints: constraint conflict, grammatical competition and the rise of periphrastic 'do'"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2003-01-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "*Optimality Theory and Language Change*, edited by D. Eric Holt. Dordrecht: Kluwer: 337–386."
publication_short: "*Optimality Theory and Language Change*, D.E. Holt (ed.), Kluwer"

abstract: "This chapter illustrates the virtues of the Optimality-Theoretic framework (Prince & Smolensky 1993) in explicating the course of syntactic change. The rise of do-support, a well-known change in the history of English, is taken as a case study. We investigate the patterns of variation inherent in linguistic change that occur as innovating forms replace conservative forms. We take the position that these periods of variation reflect competition between grammatically incompatible structures, i.e. conceptualizing variation and change in the surface structures of language as a reflection of alternation of different underlying grammars (Kroch 1989a,b; 1994), which themselves result from reanalysis by language learners (cf. Lightfoot 1991 et seq.). We argue that the notion of constraint competition inherent in Optimality Theory is advantageous in understanding language change as competition between contradictory grammatical systems. Also, we demonstrate the capacity of Optimality Theory as a means of describing systematic, grammatically-structured long-term linguistic change – particularly changes following an 'S'-curve pattern of linguistic renewal – as resulting from systematic re-ordering of precedence relationships amongst conflicting universal grammatical principles."

# Summary. An optional shortened abstract.
summary: "An optimality-theoretic analysis of the rise of *do*-support in the history of English."

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
links: 
- name: Book-website
  url: https://www.springer.com/us/book/9781402014697"

url_pdf: 'publications/slade2003-how_to_rank_constraints.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
