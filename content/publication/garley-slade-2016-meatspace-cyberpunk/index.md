---
title: "Virtual meatspace: Word formation and deformation in cyberpunk discussions"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- Matt Garley
- admin


# Author notes (optional)
author_notes:
- "Equal contribution"
- "Equal contribution"

date: "2016-10-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "*English in Computer-Mediated Communication: Variation, Representation, and Change*, edited by Lauren M. Squires, Berlin: De Gruyter: 123–148."
publication_short: "*English in Computer-Mediated Communication: Variation, Representation, and Change*, L.M. Squires (ed.), De Gruyter"

abstract: "In an initial exploration of cyberpunk glossaries found online, we noticed a variety of formation processes for cyberpunk terms: compounding (*screamsheet* 'newspaper'), clipping (*base* 'database') and acronym formation (*DNI* 'direct neural interface'), as well as fantasized borrowings (*gomi*, Japanese 'junk'). In this analysis, we are motivated by the question of which methods of word-formation are most characteristic and productive within cyberpunk discussions online from the late 1980s to the present day. This research question engages with long-standing questions in sociohistorical linguistics regarding actuation, the origin of linguistic features; and transmission, the means by which such features spread. We deal with the latter question on a subculture-wide level, engaging the question of which forms are favored in this particular subculture. We examine the ways in which words characteristic of cyberpunk are formed and deformed through diverse and complex processes, including blending around common sound/character sequences (*corpsicle*), re-spelling (*tek* for 'tech' or *cypx* for 'cyberpunk') and sequential clipping- compounding (*netrode* 'network' + 'electrode') as well as more complex creations (e.g., *teledildonics*)."


# Summary. An optional shortened abstract.
summary: "An examination of the neologisms found in cyberpunk discussions online."

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
links:
- name: Book-website
  url: https://doi.org/10.1515/9783110490817-007

url_pdf: 'publications/garley-slade-virtual-meatspace.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
