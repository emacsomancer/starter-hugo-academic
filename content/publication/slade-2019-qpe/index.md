---
title: "Quantifier particle environments"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2019-07-09T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication:  "*Linguistic Variation* 19.2: 280-351"

abstract: "I examine the set of environments in which KA-type quantifier particles appear crosslinguistically. These environments include interrogatives, disjunctions, indefinites, all of which arguably involve elements with Hamblin-type ‘alternative’ semantic values. I show that if KA-particles are assigned a uniform denotation as variables over choice functions we can account for their appearance in what otherwise appears to be a set of heterogeneous environments. Crosslinguistic and diachronic variation in the distribution of Q-particles – including, in some cases, the appearance of multiple morphologically-distinct Q-particles in different contexts – can be handled largely in terms of differing formal morphosyntactic features and/or pragmatic components of specific KA-particles. This study focuses on tracking the evolution of KA-type particles in the history of Sinhala, with comparison to other languages of the Indian subcontinent (including Malayalam and Tamil) as well as to Japanese, Tlingit, and English."

# Summary. An optional shortened abstract.
summary: "A crosslinguistic examination of the semantic and morphosyntactic properties of quantifier particles."

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
links:
- name: journal-website
  url: http://doi.org/10.1075/lv.17007.sla

url_pdf: 'https://ling.auf.net/lingbuzz/004680/current.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
