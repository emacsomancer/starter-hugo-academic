---
title: "Untydras ealle: Grendel, Cain, and Vṛtra: Indo-European 'śruti' and Christian 'smṛti' in 'Beowulf'"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2007-01-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*In Geardagum* 27: 1–32."
publication_short: "*In Geardagum* 27"

abstract: "This paper examines some of the Christian allusions in *Beowulf*. While some of these may be interpolations, the reference to Cain and Abel seems well-intergrated into the poem. On the other hand, the references to a divine flood appear on closer inspection to be grounded in Indo-European rather than Christian myth. I conclude that *Beowulf* is essentially a 'Germanic' story representing a pre-Christian world-view and that the integration of the 'Cain and Abel' mythologem into the story does not imply a large-scale adoption of Christian ideas."

# Summary. An optional shortened abstract.
summary: "An examination of apparent Christian allusions in *Beowulf* with reference to Indo-European mythology."

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
<!-- links:  -->
<!-- - name: Book-website -->
<!--   url: https://www.springer.com/us/book/9781402014697" -->

url_pdf: 'publications/slade2007-IG27c.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
