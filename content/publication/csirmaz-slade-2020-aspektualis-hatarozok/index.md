---
title: "Aspektuális határozók: Magas pozíciók  és más, alulreprezentált olvasatok [= Aspectual adverbials: High positions and  other under-represented readings]"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- Aniko Csirmaz
- admin


# Author notes (optional)
# author_notes: 
# - "Equal contribution" 
# - "Equal contribution" 

date: "2020-12-12T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "In Kenesei István, Dékány Éva, Halm Tamás &  Surányi Balázs (eds.), *Újabb eredmények a grammatikaelmélet, nyelvtörténet  és uralisztika köréből* [= *Recent advances in grammatical theory, language  history, and Uralic studies*] (Általános nyelvészeti tanulmányok [= Studies in Generative Linguistics] 32), 281–296. Budapest: Akadémiai Kiadó"
publication_short: "*Újabb eredmények a grammatikaelmélet, nyelvtörténet
 és uralisztika köréből*, Akadémiai Kiadó, 2020"

abstract: "In this paper we consider the idea that a common, templatic definition lies behind the varied meaning of aspectual adverbials (*again*, *still*, etc). We discuss the feasibility of extending the general definition to adverbials that have a pragmatically flavored interpretation. The templatic approach, combined with a proposal of structurally high, discourse-related attachment positions, provides an account of a variety of less well discussed readings of aspectual adverbials.<br/><br/>A cikk alapja az, hogy egy közös templátum áll az aspektuális határozók (*megint*, *még*, stb) különféle jelentése mögött. Amellett érvelünk, hogy az általános definiciót ki lehet terjeszteni azokra a határozókra, amiknek a jelentése legalábbis részben pragmatikai eszközökkel írható le. A templátum-alapú megközelítés és a határozók szerkezetileg magas, diskurzussal kapcsolatos helyzete lehetővé teszi a különféle aspektuális határozók leirását; a templátum segítségével nem csak a szigorúan vett időhatározói jelentéseket lehet leírni."

# Summary. An optional shortened abstract.
summary: Argues for a templatic definition for aspectual adverbs (*again*, *still*, &c) in English & Hungarian.

tags: []

# Display this page in the Featured widget?
featured: false

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
<!-- links: -->
<!-- - name: website -->
<!--   url: https://benjamins.com/catalog/atoh.16.02csi -->

url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
