---
title: "Development of verb-verb complexes in Indo-Aryan"

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here 
# and it will be replaced with their full name and linked to their profile.
authors:
- admin


date: "2021-02-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
# publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["6"]

# Publication name and optional abbreviated publication name.
publication: "*Verb-Verb complexes in Asian languages*. edited by Prashant Pardeshi, Taro Kageyama, & Peter Edwin Hook. Oxford: Oxford University Press, 249-274"
publication_short: "*Verb-Verb complexes in Asian languages*, OUP"


abstract: "One feature of modern Indo-Aryan [IA] languages – and South Asian languages more generally – is the employment of verb-verb compounds, which involve collocations of two verbs, where one (the “light” or “vector” verb) acts as a grammaticalised version of its full verb counterpart. Thus, the verb GO in many IA languages can appear as a light verb in verb-verb [VV] compounds, contributing various more functional semantic components including completion. In this chapter, I discuss the historical development and properties of VV compounds in IA, with reference to VVs in Dravidian. This includes examination of the precursors of modern Indo-Aryan verb-verb collocations, especially early examples in Sri Lankan Pali and early Sinhala."

# Summary. An optional shortened abstract.
summary: "Study of the development and features of verb-verb collocations in Indo-Aryan, with reference to Dravidian."

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org
links:
- name: Book-website
  url: https://oxford.universitypressscholarship.com/view/10.1093/oso/9780198759508.001.0001/oso-9780198759508

url_pdf: 'https://ling.auf.net/lingbuzz/005434/current.pdf'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# image:
#   caption: 'Image credit: 
# [**Unsplash**](https://unsplash.com/photos/pLCdAaMFLTE)'
#   focal_point: ""
#   preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
# projects:
# - example

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

<!-- # {{% callout note %}} -->
<!-- # Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software. -->
<!-- # {{% /callout %}} -->

<!-- # {{% callout note %}} -->
<!-- # Create your slides in Markdown - click the *Slides* button to check out the example. -->
<!-- # {{% /callout %}} -->

<!-- # Supplementary notes can be added here, including [code, math, and images](https://wowchemy.com/docs/writing-markdown-latex/). -->
