+++
title = "LaTeX in half an hour for linguists (and others)"
author = ["Benjamin Slade"]
date = 2019-07-04T22:12:26-06:00
categories = ["latex"]
tags_include = []
tags_exclude = []
draft = false
creator = "Emacs 26.2 (Org mode 9.2.4 + ox-hugo)"
+++

A self-illustrating tutorial on using LaTeX, aimed at linguists, but
it should be useful for non-linguists as well: <https://gitlab.com/emacsomancer/latex-for-linguists>

It illustrates a number of basic things: enough to cover all of the
basic things a linguist would need for writing a paper (excluding
bibliography things for the moment), and shows off some of the more
exciting typographical possibilities of LaTeX. 

The `.tex` file is directly accessible here: <https://gitlab.com/emacsomancer/latex-for-linguists/blob/master/basic-for-linguists.tex>

And the resulting `.pdf` here: <https://gitlab.com/emacsomancer/latex-for-linguists/blob/master/basic-for-linguists.pdf>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
