+++
title = "Scheming French Geeks, Eunuchs OS, Lisping & lots of Guile"
author = ["Benjamin Slade"]
date = 2018-08-04T09:46:00-06:00
tags = ["guile", "french", "scheme", "guix", "lisp"]
categories = ["lisp", "guix"]
draft = false
creator = "Emacs 26.1 (Org mode 9.1.13 + ox-hugo)"
+++

I've recently installed [GuixSD](https://www.gnu.org/software/guix/), a distribution of GNU [Linux](https://en.wikipedia.org/wiki/Linux), on one
of my machines.  GuixSD<sup>[‖](#org2d02451)</sup><a id="org65bd8e5"></a> is notable for having a number of major
components (package manager, init system) written in [GNU Guile](https://en.wikipedia.org/wiki/GNU_Guile), an
implementation of [Scheme](https://en.wikipedia.org/wiki/Scheme_(programming_language)), which is itself a dialect of [Lisp](https://en.wikipedia.org/wiki/Lisp_(programming_language))
(formerly LISP)<sup>[†](#org8188f4f)</sup><a id="org70af563"></a>. But _Guix_ is pronounced `/ɡiːks/` (i.e. identically
to _geeks_). Why?

The final _x_ of _Guix_ is presumably by analogy to _Linux_, which
  was itself produced by analogy to [_Unix_](https://en.wikipedia.org/wiki/Unix)<sup>[‡](#org7db0f64)</sup><a id="org88ffda0"></a>, based on the first name
  of its creator, [**Linus** Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds).<sup>[\*](#orgc1729f4)</sup><a id="orga223c44"></a>  Final _x_ appears in other
  software/computer names, including LaTeX (where it is not,
  however, pronounced as `/ks/` but as `/χ/` or at least `/k/`).

So what about the _Gui_ bit?  Transparently that would seem to
 derived from the initial part of _Guile_, which, however, is of
 course pronounced `/ɡaɪl/`. So how do we get to `/ɡiːks/`
 (i.e. _geeks_)?  Through French orthography (and an obvious
 intention). So in French orthography _Gui_ is pronounced `/gi/`,
 e.g. in names like _Guillaume_ (the French equivalent of English
 _William_). Likewise, _x_ in French is `/ks/`, so the whole
 combination would be realised in French as ... `/ɡiːks/`. And it's
 probably no accident that the Guix team seems to have a number of
 Francophone developers, including the project lead [Ludovic
 Courtès](http://web.fdn.fr/~lcourtes/).

What's with the tricky names of the programming languages? Guile,
 Scheme. Guile is the GNU Foundation's implementation of Scheme,
 and so the playful naming-by-synonymy makes sense. Scheme itself
 was originally _Schemer_ (where some sort of file name limitation
 resulted in its current name), following a naming convention of
 other Lisp dialects/derived languages including [PLANNER](https://en.wikipedia.org/wiki/Planner_(programming_language)) and
 CONNIVER (though where the 'tricky' naming scheme originated is
 not clear to me).

Moving away from the recent 'technological' use of these names,
 _guile_ itself is an interesting word. _Guile_ was borrowed into
 English from Old French _guile_ (though the Francophone spelling),
 with cognates in Provençal _guila_ and Portuguese
 _guilha_. _Guile_ appears early in Middle English, at least by the
 early 13th century (some representative quotes follow).

> c1230  (▸?a1200)    _Ancrene Riwle_ (Corpus Cambr.) (1962) 105   Muche gile is i vox.

<!--quoteend-->

> 13..   _K. Alis._ 1427   The thridde day, withoute gyle, He aryved at Cysile.

<!--quoteend-->

> a1400   _Sir Perc._ 1034   He was by-thoghte of a gyle.

In Middle English (and presumably Old French) it would have been
pronounced `/giːl/`; the [Great English Vowel Shift](https://en.wikipedia.org/wiki/Great_Vowel_Shift), which affected English
long vowels, resulted in modern `/gaɪl/`.

The Old French word itself has been theorised to come from a borrowing from
    (Germanic) Frankish _\*wigila_ "ruse", and thus makes _guile_
    cognate with English _wile_ "stratagem practiced for ensnaring or
    deception; a sly, insidious artifice" (usually in the plural forms
    _wiles_), which itself has somewhat dark and dubious origins,
    potentially an inheritance from native Old English, though
    possibly a borrowing from Old Norse, as the earlier attestations
    are from areas of the Danelaw of England which were obviously
    subject to heavy Scandinavian influence.

Amusingly, despite its very Francophone orthographic garb, French
  lost _guile_ at some point and so the word is absent in modern
  French (the modern equivalent seems to be French _ruse_).

[If you're interested in the technological side of Guix, have a look
at [this post over on my tech blog](https://babbagefiles.xyz/guix_maze_of_lispy_little_passages/).]

<a id="org2d02451"></a>[‖](#org65bd8e5) The 'SD' stands for 'software distribution.

<a id="org8188f4f"></a>[†](#org70af563) LISP was coined from "LISt Processor", since the list is the
   central syntactic structure, common to data and code
   representation in Lisps. Lisp is an interesting artificial
   language in that it has a number of dialects, including [Common
   Lisp](https://common-lisp.net/) (reminiscent of Tolkien's Common Tongue) as well as Scheme,
   [Racket](https://racket-lang.org/) (Scheme-related, thus the name), [Clojure](https://clojure.org/) and many
   others. The originator of Lisp, [John McCarthy](https://en.wikipedia.org/wiki/John_McCarthy_(computer_scientist)), was strongly
   influenced by lambda calculus, the formalism developed by [Alonzo
   Church](https://en.wikipedia.org/wiki/Alonzo_Church) (one of Alan Turing's mentors), which also of course is
   important in formal semantics as practised within linguistics.

<a id="org7db0f64"></a>[‡](#org88ffda0) Why Unix should be so called is a elaborate instance of word-play
    itself. It was in some ways the successor to an earlier operating
    system called _Multics_. Both Unix and [Multics](https://en.wikipedia.org/wiki/Multics) were originally
    designed to be multi-user/time-sharing operating systems for large
    expensive computers in late 60s/early 70s. _Multics_ is based on a
    straight-forward acronym of _Multiplexed Information and Computing
    Service_, while _Unix_ is a play on _Multics_ with _multi-_ being
    substituted by _uni-_, and the result sounding like _eunuchs_,
    with the suggestion of Unix being an 'emasculated' Multics.

<a id="orgc1729f4"></a>[\*](#orga223c44) Unfortunately _Linux_ is not based on what would be the more
    interesting recursive acronym "Linux Is Not UniX" – unlike _GNU_,
    which is indeed the recursive acronym "GNU is Not Unix".

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
