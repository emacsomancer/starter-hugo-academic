+++
title = "Of typewriters, runes, and short-hand"
author = ["Benjamin Slade"]
date = 2018-06-19T14:18:00-06:00
publishDate = 2018-06-18T00:00:00-06:00
images = ["images/132v-selection-highlight.jpg", "images/141v-ethel.jpg", "images/remington02-keyboard.jpg", "images/remington-standard-model-2-1878.jpg", "images/USASCII_code_chart.png", "images/Tironian_et_shapes.jpg"]
tags = ["orthography", "tironian", "old-english", "irish", "shorthand", "serendipity"]
categories = ["orthography"]
draft = false
creator = "Emacs 28.0.50 (Org mode 9.4 + ox-hugo)"
+++

Or, _why is the `&` on the `7` key?_

Back in ca. 2003, when I was preparing the diplomatic edition of _Beowulf_ for
[Beowulf on Steorarume (heorot.dk)](https://heorot.dk), I noticed the use of what looks like
the Arabic numeral '7' for _and_ (Old English _ond_).


## _Beowulf_ f.132v: ll.120b-123a {#beowulf-f-dot-132v-ll-dot-120b-123a}

{{< figure src="132v-selection-highlight.jpg" >}}

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style type="text/css">
    /* Left-aligned text in a centered block (does not work in Adobe Digital Editions 1.7) */
    .poem {
      display: table;
      margin-right: auto;
      margin-left: auto;
    }

    /* Indent wrapping lines */
    .stanza {
      text-indent: 0;
      margin-left: 2em;
    }
    .verse {
      margin-left: -2em;
    }
  </style>
</head>

<body style="font-size: 75%;">
<div style="float: left; width: 50%;">
<ul>
    <p class="stanza"><span class="verse">wiht unhælo</span><br />
      <span class="verse">grim <span style="color:red">⁊</span> grædig    gearo sona wæs</span><br />
      <span class="verse">reoc <span style="color:red">⁊</span> reþe    <span style="color:red">⁊</span> on ræste genam</span><br />
      <span class="verse">þritig þegna</span>
    </p>
</ul>
</div>
<div style="float: right; width: 50%;">
<ul>
    <p class="stanza"><span class="verse">The unholy creature,</span><br />
      <span class="verse">grim <span style="color:red">and</span> greedy,      soon was geared-up,</span><br />
      <span class="verse">wild <span style="color:red">and</span> savage;      <span style="color:red">and</span> seized from their sleep</span><br />
      <span class="verse">thirty thanes</span>
    </p>
  </div>
  </ul>
</body>
</html>

This turned out to be a [Tironian _et_ (⁊)](https://en.wikipedia.org/wiki/Tironian%5Fnotes) , one of a number of
shorthand symbols devised by Marcus Tullius Cicero's slave and
ppersonal secretary Tiro in the first century which continued to be
used by monastic scribes in the medieval period. The more familiar
abbreviation of _et_ "and" is of course the ampersand `&`, which,
unlike `⁊`, is in fact a stylised _et_. (For more discussion of some
of the back history of both the ampersand and the Tironian _et_ see
these [Shady](https://web.archive.org/web/20180318061514/www.shadycharacters.co.uk/2011/06/the-ampersand-part-1-of-2/) [Characters](https://web.archive.org/web/20180423230942/http://www.shadycharacters.co.uk/2011/06/the-ampersand-part-2-of-2/) [posts](https://web.archive.org/web/20180329153622/http://www.shadycharacters.co.uk/2011/07/the-ampersand-part-2%25c2%25bd-of-2/).)


## Other shapes of the Tironian et {#other-shapes-of-the-tironian-et}

{{< figure src="Tironian_et_shapes.jpg" >}}

The fact that the Tironian _et_ `⁊`, a shorthand 'quick' symbol like
the ampersand `&`, looks like an Arabic numeral `7` makes the
appearance of the `&` above the `7` (i.e. as the shifted value of the
`7`) seem significant. At the time of creating the diplomatic web
edition of _Beowulf_ I assumed that this must be the reason for the
particular placement of the `&` on keyboards.

Unsurprisingly, other people have thought of this as well, including a
[StackExchange question](https://english.stackexchange.com/questions/200710/did-the-tironian-et-have-any-impact-on-the-ampersand-being-shift-7-on) (as well as [a comment on a Shady Characters
post on ampersands](https://web.archive.org/web/20180318061514/http://www.shadycharacters.co.uk/2011/06/the-ampersand-part-1-of-2/#comment-699)). There the 'accepted answer' to this question says:

> Although the `7` was the ampersand on IBM's standard keyboard layout,
> that is hardly universal. The first nine printable characters in ASCII
> are `!` `"` `#` `$` `%` `&` `'` `(` `)`, which should give a good clue
> as to what the top row of a teletype keyboard looked like. On many
> early teletypes and terminals (and also, BTW, on the Apple ][), the
> shift key toggled bit 4 of the character being produced, thus it would
> turn a `1` (011 0001) into `!` (010 0001), and `,` (010 1100) into `<`
> (011 1100). Since the digits 1–9 received consecutive code, so did the
> characters produced by combining them with the shift key. `Shift-7` on
> those keyboards was apostrophe; the ampersand was `shift-6`.
>
> Other typewriter keyboards also varied considerably in where they put
> the ampersand. Its association with the number `7` is nowhere near as
> consistent as the association between `1` and `!`, `3` and `#`, `4`
> and `$`, or `5` and `%` which existed in both older computer keyboards
> and today's US arrangement.

{{< figure src="USASCII_code_chart.png" >}}

The association of `!` ('bang') with `1` actually has a fairly tidy
explanation as many of the early typewriters omitted the numeral `1`
and the typist was expected just to use the lowercase letter `l` in
its place; likewise the `!` was omitted and was also typed using the
lowercase `l` then backspacing and typing a full-stop `.`.

While [ASCII](https://en.wikipedia.org/wiki/Ascii) and older computer keyboards may not provide evidence for
early association of `7` and `&`, an examination of the early
typewriters from [antiquetypewriters.com](http://www.antiquetypewriters.com/collection) reveals firstly that almost
all of the early typewriters collected there which have symbols as the
result of 'shifted' numerals do in fact place `&` with the `7`
including the [1892 Norths typewriter (London)](http://www.antiquetypewriters.com/collection/enlarged.asp?img=pic-norths.jpg&typewritername=Norths), Wagner Typewriter Co's
(New Jersey) [1896 Underwood 1](http://www.antiquetypewriters.com/collection/typewriter.asp?Underwood%25201), the [1898 Shimmer (Milton,
Pennsylvania)](http://www.antiquetypewriters.com/collection/typewriter.asp?Shimer), Meiselbach Typewriter Co.'s (Kenosha, Wisconsin, USA)
[1901 Sholes Visible](http://www.antiquetypewriters.com/collection/typewriter.asp?Sholes%2520Visible), and, secondly, and crucially, the [1878 Remington
Standard Model No. 2](http://www.antiquetypewriters.com/collection/detailed.asp?img=pic-remington02.jpg&typewritername=Remington%25202) (New York) also positions `&` as the shifted
value of `7`. The Remington Standard No. 2 is [described](http://americanhistory.si.edu/collections/search/object/nmah%5F687313) as the first
commercially successful typewriter, and its (QWERTY-style) layout thus
became established as standard.


## Remington Standard No. 2 {#remington-standard-no-dot-2}

{{< figure src="remington-standard-model-2-1878.jpg" >}}

{{< figure src="remington02-keyboard.jpg" >}}

The one early typewriter I found with shifted numerals as symbols which
**didn't** associate `7` and `&` is the [1895 Waverly](http://www.antiquetypewriters.com/collection/typewriter.asp?Waverley) (London) where the
`&` is above the `6` and the `7` has a `/` above it.

So an association between `&` and `7` for keyboard does seem to have
been established in the 19th century, presumably through the success
of the Remington Standard No. 2. The question then shifts to whether
or not the placement of the `&` above the `7` by the designers of the
Remington Standard No. 2 was motivated by Tironian sensibilities or
not.

The Tironian _et_ has disappeared from most everywhere, excepting in
[modern Irish typography](https://stancarey.wordpress.com/2014/09/18/the-tironian-et-in-galway-ireland/). Interestingly, [some Irish typewriters](https://web.archive.org/web/20170214010810/http://evertype.com/celtscript/type-keys.html) place
the Tironian _et_ `⁊` above the `7` key.

Keith Houston at the Shady Characters website [suggests](https://web.archive.org/web/20180423230942/http://www.shadycharacters.co.uk/2011/06/the-ampersand-part-2-of-2/#paperref%5F16) that:

> the Tironian notes suffered near-extinction in the Middle Ages,
> victim of a curious linguistic witch hunt. The secrecy and
> cipher-like nature of both traditional runic writing and
> short­hand did not sit well with the distrust of witch­craft and
> magic prevalent in those times, and Tiro’s system fell out of use.

Indeed, in Old English manuscripts, runes sometimes appear, standing
in for the words which are borne by the particular runes as names,
including [ᛗ](https://en.wikipedia.org/wiki/Mannaz) (_mann_ "man"), [ᛞ](https://en.wikipedia.org/wiki/Dagaz) (_dæg_ "day"), and [ᛟ](https://en.wikipedia.org/wiki/Odal%5F(rune)) (_œþel_ "ancestral
land"). The last of these is used three times in the extant text of _Beowulf_.


## _Beowulf_ f.141v: ll.520–521 {#beowulf-f-dot-141v-ll-dot-520-521}

{{< figure src="141v-ethel.jpg" >}}

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style type="text/css">
    /* Left-aligned text in a centered block (does not work in Adobe Digital Editions 1.7) */
    .poem {
      display: table;
      margin-right: auto;
      margin-left: auto;
    }

    /* Indent wrapping lines */
    .stanza {
      text-indent: 0;
      margin-left: 2em;
    }
    .verse {
      margin-left: -2em;
    }
  </style>
</head>

<body style="font-size: 75%;">
<div style="float: left; width: 50%;">
<ul>
    <p class="stanza"><span class="verse">ðonon he gesohte   swæsne <span style="color:red">·ᛟ·</span><br />
      <span class="verse">leof his leodum    lond brondinga</span>
    </p>
</ul>
</div>
<div style="float: right; width: 50%;">
<ul>
    <p class="stanza"><span class="verse">then he sought his own sweet <span style="color:red">·ancestral homeland·</span></span><br />
      <span class="verse">– loved by his people –  the land of Brondings </span>
    </p>
  </div>
  </ul>
</body>
</html>

In any event, what of the question of "why is the `&` on the `7` key?"

It would seem in all likelihood that the placement of the `&` on the
`7` is a happy accident. Outside of Ireland, Tironian ⁊s would have
been long enough out of fashion that it seems unlikely anyone (or, at
least, typewriter engineers) would have associated `&` with `7`. The
positioning of the Tironian `⁊` above the `7` key on some Irish
typewriters, on the other hand, is then doubly predestined, given the
largely standard positioning of the semantically-equivalent `&` over
the `7` on many typewriters and the similarity in appearance of the
Arabic `7` numeral and the Tironian `⁊` symbol.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
