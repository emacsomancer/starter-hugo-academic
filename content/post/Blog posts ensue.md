+++
title = "Blog posts ensue"
author = ["Benjamin Slade"]
date = 2018-06-07T21:13:00-06:00
tags = ["misc"]
categories = ["admin", "nepali"]
draft = false
creator = "Emacs 26.1 (Org mode 9.1.13 + ox-hugo)"
+++

I'm still working on the back-end technology, but hopefully more blog
posts should start appearing here soon(ish), acting as a (prettier and
less ~~privacy-invading~~ blogspot-ish) continuation of my old
linguistics blog, [Stæfcræft & Vyākaraṇa](https://staefcraeft.blogspot.com/).

If you're interested in my ~~musings~~ scribblings on topics outside of
natural language, you might visit my other blog, [The Neo-Babbage
Files](https://babbagefiles.xyz).

In the meantime, here's a clever Nepali tongue-twister my wife came up
with:p

> पारसीले परसि फरसि खान्छ रे
>
> _pārsī parsi pharsi khāncha re_
>
> "[They say:] A/The Parsi (Zoroastrian) will eat (some) pumpkin the day after tomorrow."

पारसी (pārsī) is "[Parsi](https://en.wikipedia.org/wiki/Parsi)", परसि (parsi) is "the day after tomorrow",
and फरसि (pharsi) is "pumpkin" (often in the sense of a pumpkin
dish). (रे (re) is a Nepali quotative particle.)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
