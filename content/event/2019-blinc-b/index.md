---
title: "The Visibility Parameter Reconsidered"

event: '3rd Budapest Linguistics Conference [BLINC-3]'
event_url: "https://lassolsuchll2019.weebly.com/"

location: Eötvös Loránd University, Budapest

summary: "An examination of unusual low readings of elements like *again* and *almost* in the context of the Visibility Parameter."

abstract: "[http://seas3.elte.hu/blinc3/Abstracts/Talks/Csirmaz&Slade.pdf](http://seas3.elte.hu/blinc3/Abstracts/Talks/Csirmaz&Slade.pdf)"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2019-06-06T17:15:00"
date_end: "2019-06-06T18:00:00"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- Aniko Csirmaz
- admin

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

