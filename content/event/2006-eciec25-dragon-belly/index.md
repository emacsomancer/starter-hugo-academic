---
title: "Splitting the Belly: the Indo-European Dragon-Slaying Myth in Indic & Germanic"

event: "25th East Coast Indo-European Conference [ECIEC 25]"
event_url: 

location: The Ohio State University, Columbus

summary: A proposal for other Proto-Indo-European formulae associated with Watkins' dragon-slaying formula.

abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2006-06-01T00:00:00"
# date_end: "2016-08-02T19:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

