---
title: 'An Optimality-Theoretical Analysis of a Novel Morphological Process in Rasta Talk'

event: 'Meaning in Flux 2017'
event_url: https://ling.yale.edu/research/meaninginflux2017

location: Yale University, New Haven, Connecticut

summary: "A formal account which explains crosslinguistically-recurrent morphological & etymological connections between different temporally-associated adverbs."

abstract: "We explore the historical relations of different adverbs with connections to the temporal realm, including elements corresponding to English *then*, *again*, various senses of *still*, and interactions with additive and scalar particles, focussing on Hungarian, Hindi, and Nepali. Connections between adverbs like THEN and AGAIN and STILL in Hindi/Nepali and Hungarian suggest an underlying generalised ordering adverbial, for which we provide a formalisation. Of particular interest is the appearance of additive particles in the formation of a number of temporal (and non-temporal) adverbials, found in Hungarian, Hindi, and Nepali, where we find the form used for 'concessive' STILL is essentially composed of 'again'+additive particle. Hindi *bhī* and Nepali *pani* are additive/scalar particles often meaning something like 'also, even'. Capturing the interrelations between ordering adverbials in languages like Hindi/Nepali and Hungarian is facilitated by the positing of a generalised ordering adverbial. Here, for instance, *again* and *then* are identical, save for the position of focus and thus the resulting focus alternatives. This also accounts for the apparent homophony of THEN and AGAIN in Nepali *pheri*, Hindi *phir*, which can rather be thought of as a type of polysemy."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2017-10-14T09:50:00"
date_end: "2017-10-14T10:10:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Aniko Csirmaz

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

