---
title: "Anatomy of Hungarian aspectual particles"

event: 'International Conference on the Structure of Hungarian [ICSH13]'
event_url: ""

location: Research Institute for Linguistics of the Hungarian Academy of Sciences, Budapest

summary: "An examination of the inner structure of Hungarian aspectual particles like *mégis*."

abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2017-06-01T00:00:00"
# date_end: "2019-06-06T18:00:00"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- Aniko Csirmaz
- admin

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

