---
title: 'Visibility parameter: An unusual class of adverbials'

event: '47th Meeting of the Linguistic Association of the Southwest [LASSO2018]'
event_url: https://linguistics.byu.edu/faculty/deddingt/LASSO2018/

location: Aspen Grove, Provo, Utah

summary: "An experimental re-examination of the Visibilty Parameter in English suggests a higher accessibility of low readings than previously supposed."

abstract: "A small set of adverbials permit an unusual, ​ low reading. In English, *again* and *almost* allow the low reading (e.g. *Bill almost closed the door* as 'Bill did something, the result of which was the door being almost closed'). For other adverbials, this is unavailable (*Bill closed the door twice* ≠ 'Bill did something which resulted in the door being closed twice'). Surprisingly, some equivalents of ​ *again*/*almost* ​ also have low readings in other languages. This study  explores the availability of low readings in English. We hypothesise that the aspectual class of the verb/predicate plays a role in whether the low reading is permissible. Testing the role of aspectual classes is important because aspectual properties are often relevant for temporal/aspectual particles. The default hypothesis should thus be that aspectual properties are relevant for *again* as well. Participants judge the availability of low readings on a 4-point Likert-scale, where the context forces the low reading. Preliminary results suggest that low readings are fairly productive (contrary to Scholler 2014). However, while verbs of motion with a goal PP were judged as perfect, other verbs of motion structures and causatives appear more marked; aspect apparently matters. Currently we are testing whether there is a significant difference between low readings when comparing various aspectual subclasses (e.g. different resultatives), using one-way ANOVA. Subsequently we will test whether there is a significant difference in the availability of low readings among broader aspectual classes (e.g. goal PPs and resultatives). Our results indicate that the low reading is fairly productive in English."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2018-10-12T10:15:00"
date_end: "2018-10-12T10:45:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- Aniko Csirmaz
- admin


# Author notes (optional)
author_notes:
- "Equal contribution"
- "Equal contribution"

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

