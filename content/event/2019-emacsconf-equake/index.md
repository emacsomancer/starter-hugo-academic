---
title: 'Equake - drop-down console written in elisp'

event: emacsconf2019
event_url: https://emacsconf.org/2019/

location: emacsconf online - https://emacsconf.org/2019/

summary: 'A demonstration of a drop-down console written in Emacs Lisp.'
abstract: "Brief demonstration (5 minute) of the `equake` package, a drop-down console in the style of Yakuake/Guake for (e)shell and terminal emulation, implemented in Emacs Lisp. Equake provides access to various shells and terminal emulators, including Emacs&rsquo; own `shell` (an Emacs wrapper around the current system shell), `term` and `ansi-term`, (both terminal emulators, emulating VT100-style ANSI escape codes, like xterm does), and `eshell` (a shell written entirely in Emacs Lisp); with experimental support for `rash` (a shell written in Racket (=PLT Scheme)). Equake provides monitor-specific tabs, which can be renamed and re-ordered. Frame splitting of course is supported by default in Emacs. Each tab can contain a different shell or terminal emulator, thus one could run `eshell` in one tab, `ansi-term` in another tab, `rash` in a third tab, &c. and quickly switch between them."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2019-11-02T18:00:00Z"
date_end: "2019-11-02T18:10:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: "https://media.emacsconf.org/2019/30.html"

---

