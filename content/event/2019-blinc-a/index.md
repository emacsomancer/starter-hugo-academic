---
title: "The Uniform Treatment of Adverbials Hypothesis"

event: '3rd Budapest Linguistics Conference [BLINC-3]'
event_url: "https://lassolsuchll2019.weebly.com/"

location: Eötvös Loránd University, Budapest

summary: "An argument for a microsemantic/templatic treatment of aspectual adverbials."

abstract: "[http://seas3.elte.hu/blinc3/Abstracts/Talks/Slade&Csirmaz.pdf](http://seas3.elte.hu/blinc3/Abstracts/Talks/Slade&Csirmaz.pdf)"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2019-06-07T09:30:00"
date_end: "2019-06-07T10:15:00"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Aniko Csirmaz


tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

