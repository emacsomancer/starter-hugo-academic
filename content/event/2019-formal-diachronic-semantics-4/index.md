---
title: "Universality and the evolution of aspectual adverbials"

event: 'Formal Diachronic Semantics 4'
event_url: "https://u.osu.edu/fods4/"

location: The Ohio State University, Columbus

summary: "A proposal for a templatic approach to aspectual adverbials."

abstract: '**<u>Templates.</u>** We argue, focussing specifically on aspectual adverbials including *again*, *still*, *then*, that some linguistic elements form a network and are best described as having a basic templatic definition. The template allows variation in linguistic realizations and accounts for patterns of polysemy. We show that realizations can be derived from a variety of related items historically, and that crucially the earlier forms lack any templatic component.

*<u>Templatic meaning for aspectual adverbials.</u>* There are a variety of items which are amenable to a templatic treatment. Consider aspectual adverbials. Löbner 1989 and Krifka 2000 propose a system of aspectual adverbials that are related by inner and outer negation:

The proposal above addresses temporal interpretation (*Fred is still / already sleeping*). Several adverbials, most notably *still* and its equivalents have usages that go beyond temporal interpretation. As dicussed by Beck 2018 a.o., German *noch* ‘still’ and its equivalents permit a variable range of other readings, briefly illustrated below.

Reineland in still in Canada (spatial, also for *already*) .̱ He felt sick, but he still decided to stay (concessive) This dress is still expensive (marginal)

While some readings, including , merely involve a scale distinct from the temporal scale, others (e.g.  concessive, marginal involve a more significant difference (see Beck 2018, a.o.). Thus *still, noch* can be said to involve polysemy.

Polysemy is not restricted to *still*. The aspectual adverbials below include repetitives, ordering *then* (*And then he left*) and its inverse. A number of items that are distinct in English have identical realizations:
<table>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">‘before</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><em>temp.</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">‘(not)</td>
<td style="text-align: left;">‘(not)</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">that’</td>
<td style="text-align: left;">‘then’</td>
<td style="text-align: left;">‘again’</td>
<td style="text-align: left;">‘still’</td>
<td style="text-align: left;">‘already’</td>
<td style="text-align: left;">yet’</td>
<td style="text-align: left;">anymore’</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Hindi <em>ab tak</em>, Nepali <em>ahile samma</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;">(<span>X</span>)</td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">Hindi <em>phir</em>, Nepali <em>pheri</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">X</td>
<td style="text-align: left;">X</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Romanian <em>mai</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
</tr>
<tr class="even">
<td style="text-align: left;">Italian <em>ancora</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Jamaican patois <em>aredi</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">Spanish <em>ya</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Spanish <em>todavia</em></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">Hungarian <em>még</em></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">German <em>noch</em></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><span>X</span></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

The polysemy of the various aspectual adverbs suggests a single underlying definition; the shared definition allows different meanings to be realized by the same element. At the same time, there is no expectation that there is a common element for all uses, as shown by the empty cells and by the first row of the table. Darker coloured cells indicate patterns which are unexpected on a Löbner-style approach.

*<u>Template.</u>* We propose that the underlying meaning is a template.  Here *x*, *x*′ are scalar entities (times, degrees, etc) such that *x*′ precedes *x* on scale *S*; *P*, *Q* are saturated predicates except for the arguments indicated, *F**A* is a set of focus alternatives to *P*(*x*,…) which differ in the elements under focus (times, degrees, or subconstituents). The aspectual adverbials can differ in the identity of the scale; the type of argument; whether *P* and *Q* are identical; the identity of the focused element (e.g. for repetitives (*again*) the time argument must be focused); and the relation (e.g. immediate precedence for *still* and *already*, simple precedence for *again*, ordering *then* and *before that*).

. .<span id="template" label="template">\[template\]</span> =  
*λ**x*<sub>*S*</sub>*λ**e**λ**P* : ∃*x*<sub>*S*</sub><sup>\*</sup>∃*e*<sup>\*</sup>∃*Q*\[*Q*(*e*<sup>\*</sup>,*x*<sup>\*</sup>,…)&*Q*(*e*<sup>\*</sup>,*x*<sup>\*</sup>,…)∈*F**A*(*P*(*e*,*x*,...))&*x*<sup>\*</sup>*R**x*\].*P*(*e*,*x*,…)
.̱ =
*λ**t*<sub>*T*</sub>*λ**e**λ**P* : ∃*t*<sub>*T*</sub><sup>\*</sup>∃*e*<sup>\*</sup>∃*Q*\[*Q*(*e*<sup>\*</sup>,*t*<sup>\*</sup>,…)&*Q*(*e*<sup>\*</sup>,*t*<sup>\*</sup>,…)∈*F**A*(*P*(*e*,*t*,...))&*t*<sup>\*</sup>∝*t*\].*P*(*e*,*t*,…)

The template offers a unique underlying definition. Whenever a surface
form has different aspectual interpretations, that form is more general
(the unmarked form), and other aspectual adverbs are more specific, but
based on the template. Various interpretations of *still* (e.g. marginal
and concessive readings) also fit the template.

*<u>Already.</u>* We reanalyze *already* as being similar to *still*,
but involving an inverted time scale (so *x*<sup>\*</sup> follows rather
than precededs *x*). Thus the truth of *P* at a preceding time is
presupposed for *still*, but ¬*P* implicated for *already*.

*<u>Focus sensitivity.</u>* The template appeals to focus alternatives
to determine predicates in the presupposition. Beck 2018 argues that
*still, noch* are not focus-sensitive. Rather, they tend to occur in
environments where focus is likely to occur independently. Beck 2018
points out unlike *nur/only* and *auch/also*, *noch/still* does not
appear to be able to associate with focus alternatives within syntactic
islands. We provide examples showing that this association is possible
for examples where a scale is clearly available for ranking
alternatives.

**<u>Historical development</u>**

Crosslinguistically, aspectual adverbials often have similar
etymologies. At the same time, the earlier usage lacks a templatic
component, which is expected if the template is available for functional
or semi-functional elements.

*<u>Repetitives.</u>* Historically, we can observe a number of
interesting trends in repetitives, including a recurrent pattern of
elements which develop ultimately from words meaning “hinder-part” to
the adverbial “back” and thence to “again”. This includes Kutchi
Gujarati *pacho* “again (repet.& restit.) **&** back” (see Patel-Grosz &
Beck 2014) \< OIA.*\*paśca-* “hinder part” \[Turner 1966: #7990\] as
well as English *back* itself. In the case of English *again*, this word
originally meant “back, in the opposite direction” (=OE *ongean*):

. “He sceaf þa mid ðam scylde, ðæt se sceaft tobærst, and þæt spere
sprengde, þæt hit sprang **ongean**.” \[“He shoved then with shield so
the shaft burst — the spear broke and sprang **back**.”\](*Battle of
Maldon* 137)

Old English *eft* (cognate with modern English *after* and *aft*) also
exhibits polysemy analysable as underspecification similar to that found
in Hindi *phir* and Nepali *pheri* in their polysemous senses of “then
(=after that)” and “again”:

. Efterward me ssel þerne mete **eft** chyewe ase þe oxe þet...
“Afterward one shall chew this food **again** like the ox
that…\[repetitive reading\]

. þone mon **eft** on Cent forbærnde. “That man was afterwards burned in
Kent.” \[*AS Chron.*685 (Parker)\]

In Hungarian, a non-IE language, the repetitive forms *megint*, *ismét*
are etymologically related to *meg* ‘back’.

Repetitives can also be derived from an expression meaning ‘new’
(English *anew, afresh*, Spanish *de nuevo*, Hungarian *újra*
‘new+onto’), though English *anew* carries additional pragmatics not
found in *de nuevo*.

Hindi *vāpas* “back” on the other hand has not (yet) developed any
repetitive senses, and represents loanword from Persian, with the *pās*
part being cognate with Old Indo-Aryan *\*paśca-* “hinder-part” \[Platts
1884:1171\] (and thus is cognate ultimately with Kutchi Gujarati
*pacho*).

Hindi *phir* “then, again”, Nepali *pheri* is related to Hindi *phirnā*
“to turn”, which derives from a reconstructed Old Indo-Aryan *\*phirati*
“moves, wanders, turns”, cp.Prakrit *phiraï* “goes, returns” (Turner
1966: #9078).

In early Indo-Aryan we find Sanskrit *púnar*, ultimately underlying
Nepali *pani* (Nepali *pani* derives from Sanskrit *punar api* “even
again; again too; moreover; also” (Turner 1966:#8274)). *Púnar*, itself
an aspectual adverbial, is of interest due to being more underspecified
than many other examples, polysemous between “back; again; further;
(concessive) still”.

*<u>Still.</u>* English *still* provides an instructive view into
historical developments affecting aspectual adverbials. Originally
meaning “motionless” (still possible in Mod Eng), it has come in Modern
English to have a great range of senses (cf.Ippolito 2007, Beck 2018).
From original sense, it developed in the 14th century an additional
possible meaning “always” (archaic by the 19th-c.), as in *Thus haue I
prov’d Tobacco good or ill; Good, if rare taken; Bad, if taken
**still**.* \[1617 R.Brathwait *Smoaking Age*\]

Only from the 16th-century do we find the modern day temporal *still*
sense, e.g.*For as you were when first your eye I eyde, Such seemes your
beautie **still**.* \[1609 Shakespeare *Sonnets* civ.sig.G2v\]

While the comparative sense appears consistently only from the 18th-c.:
*The Woodmongers Abuse..of a former Charter leaves **still** less Reason
to fear they shou’d succeed.* Concessive *still* likewise only appears
from the 18th-century: *‘Tis true, St. Giles’s buried two and thirty,
but **still** as there was but one of the Plague, People began to be
easy.* \[1722 D.Defoe *Jrnl.Plague Year* 7\]

The reanalysis from “motionless” to “always” results in a
temporally-associated adverb, whose denotation we can roughly formalise
as:

. *λ**P*∀ relevant times *t*′.*P*(*t*′)

Note that unlike the aspectual adverbials this does not involve a
presuppositional component. Thus the later 17th-c.re-analysis as a
temporal aspectual adverbial still involves a significant change in
semantic value.

Thus in all of these examined cases of the development of lexical items
into aspectual adverbials, a major semantic shift is involved. None of
these involve a sort of gradual semantic change, but rather
‘catastrophic’ reanalyses, whose frequent and crosslinguistic occurrence
strongly points to the templatic aspectual adverbial being a
universally-accessible semantic chunk.

**<u>Extensions.</u>** We discuss another possible use of templates,
personal and demonstrative pronouns. We also explore the relationship
between templates and the standard notion underspecification
(Patel-Grosz and Grosz 2017 treat pronouns as involving
underspecification, where demonstratives have more structure than
personal pronouns). Templates give rise to a type of underspecification
where individual lexical items can (a) encode particular choices for
parts of the definition such as the identity of the scale involved or
(b) have a more general definition, which permits a unique lexical entry
with a variety of lexical meanings.
'

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2019-11-16T14:15:00Z"
date_end: "2019-11-16T14:55:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Aniko Csirmaz
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: "slides/slade-csirmaz-fods4-slides.pdf"
url_video: ""

---

