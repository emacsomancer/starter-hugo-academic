---
title: "History of kakarimusubi-like constructions in Sinhala and Dravidian"

event: "Kakari-musubi from a Comparative Perspective"
event_url: 

location: National Institute for Japanese Language and Linguistics (NINJAL), Tachikawa City, Tokyo

summary: An examination of particle constructions in Sinhala and Dravidian, with comparison to Japanese.

abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2015-09-01T00:00:00"
# date_end: "2016-08-02T19:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

