---
title: "Adding meaning to Indo-Aryan aspectual adverbials ‘then’ and ‘again’"

event: '23rd International Conference on Historical Linguistics: Workshop on Logical Vocabulary & Logical Change'

event_url: https://mitrovic.co/logvoc/

location: "Hotel Contessa, San Antonio, Texas"

summary: "An analysis of aspectual adverbials in Hindi & Nepali which provides an account of the meaning which captures the inter-relations signalled by their compositional morphology."

abstract: "We examine a subset of aspectual adverbs in Hindi & Nepali and provide an account of their meaning which captures the inter-relations signalled by their compositional morphology. Specifically we examine Hindi *phir* (ambiguously ‘then, after that’ or ‘again’) and *phir bhī* (‘still’,both temporal and concessive), as well as their Nepali counterparts *pheri* and *pheri pani*. Of particular interest are the ‘still’ adverbials which involve the combination of the temporal Hindi *phir*/Nepali *pheri* with an additive/scalar particle (Hindi *bhī*/Nepali *pani*), the latter having a distribution partially overlapping with other additive/conjunctive/universal particles like Japanese *mo*, Malayalam *um*, Sinhala *t* (cp. Slade 2011, Mitrović 2014, Szabolsci 2015)."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2017-08-03T13:35:00"
date_end: "2017-08-03T13:55:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Aniko Csirmaz

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: "https://mitrovic.co/papers/slade-csirmaz-indo-aryan-hungarian-pieces.pdf"
url_video: "https://www.youtube.com/watch?v=m_pGHeh3wdE&feature=youtu.be&t=1m5s"

---

