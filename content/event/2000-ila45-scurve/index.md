---
title: "Catastrophes at 1000 Grammars/Hour on the ‘S’-Curve"

event: "45th annual meeting of the International Linguistic Association [ILA 45]"
event_url: 

location: Georgetown University, Washington, D.C.

summary: An Optimality-Theoretic account of the rise of *do*-support in English.

abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2000-04-01T00:00:00"
# date_end: "2016-08-02T19:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

