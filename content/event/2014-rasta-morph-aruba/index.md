---
title: Formal analysis of novel morphological processes in Rasta talk

event: 'Society for Caribbean Linguistics - 20th Biennial Conference'
event_url: http://www.scl-online.net/Conferences/2014/

location: Holiday Inn Resort & Casino, Palm Beach, Oranjestad, Aruba

summary: A formal account of overstandings and I-words in Rasta talk.

abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2014-08-01T00:00:00"
# date_end: "2016-08-02T19:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

