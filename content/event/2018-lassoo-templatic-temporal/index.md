---
title: 'A templatic treatment of temporal terms'

event: '47th Meeting of the Linguistic Association of the Southwest [LASSO2018]'
event_url: https://linguistics.byu.edu/faculty/deddingt/LASSO2018/

location: Aspen Grove, Provo, Utah

summary: "A proposal for a templatic meaning underlying a large class of aspectual adverbials, supported by semantic and morphological evidence from Hindi, Nepali, and Hungarian."

abstract: "We examine the internal structure of a subclass of adverbials including several temporal adverbs, in Hungarian, Hindi, and Nepali, with comparison to German and different stages of English. Connections between adverbials like *again* and *still* in Hindi, Nepali, and Hungarian suggest an underlying generalised relational adverbial, for which we present a templatic formalisation. This follows in the tradition of research which seeks to unite the different meanings of English *still* (e.g. Michaelis 1993, Beck 2016); we extend this to include *again* and *then*. The interaction of ordering adverbials and additive particles, exemplified by the 'concessive' *still* forms Hindi *phir bhī*, Nepali *pheri pani*, Hungarian *mégis*, point to further functions of additives beyond what has been previously discussed.  Positing an underlying basic template captures the morphological and semantic interrelations between ordering adverbials in languages like Hindi/Nepali and Hungarian. The morphological similarity, which links different adverbials in different languages, is taken to reflect a single underlying meaning. This view of temporal adverbials is reminiscent of Kayne's (2016) suggestion for functional items: if two functional items are homophones, they cannot have the same spelling."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2018-10-12T11:45:00"
date_end: "2018-10-12T12:15:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Aniko Csirmaz


# Author notes (optional)
author_notes:
- "Equal contribution"
- "Equal contribution"

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

