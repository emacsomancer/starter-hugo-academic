---
title: "Historical Linguistics & Language Maintenance: compound verbs, absolutives & verbal particles in Nepali, Norse & Sanskrit"

event: "26th All India Conference of Linguists [AICL 26]"
event_url: 

location: North-Eastern Hill University, Shillong, Meghalaya, India

summary: A comparison of the historical developments of Indo-Aryan compound verbs (V-Vs) and Germanic verb-particle constructions.

abstract: ""

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2004-12-01T00:00:00"
# date_end: "2016-08-02T19:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

