---
title: "Spanish Aspectual Adverbials"

event: '48th Annual Meeting of the Linguistic Association of the Southwest and ​XXX Biennial Louisiana Conference on Hispanic Languages and Literatures [LASSO-48]'
event_url: "https://lassolsuchll2019.weebly.com/"

location: Louisiana State University, Baton Rouge

summary: "An examination of Spanish aspectual adverbials."

abstract: "Krifka (2001), based on Löbner (1989), proposes a crosslinguistic account of aspectual adverbials. We consider Spanish *todavía* 'still', *ya* 'already' and *aún* 'still' and discuss some challenges Spanish raises for Löbner/Krifka. Krifka (2001) discusses temporal uses in terms of presuppositions and assertions: e.g. *still P* asserts that $P$ holds at $t$ and presupposes that there is a time $t'$ immediately preceding $t$ where $P(t')$ (*he is still sleeping*). The model can be extended to other scales (the scale is distance in *San Diego is still in the US*) (Beck 2016). <br/> <br/>**Ya.** Debelcque&Maldonado (2011) discuss *ya*. They present *ya* as a pragmatic anchor, grounding the eventuality with respect to time and movement across a ‘dynamic programmatic base’. We argue that our alternative account is more defined and offers specific predictions. First, we claim that *ya* can appear with covert predicates *P*, and is consistent with Löbner/Krifka. This accounts for *Había tortillas, frijoles, y ya* ‘There were tortillas, beans and that was all’, where *P* is *era todo* ‘that was all’. Covert predicates also account for the meaning variation when only *ya* is the overt (e.g. *¿Ya?*). Second, we discuss examples where *ya* is equivalent to clause-final occurrences of *already*, a modal use (*Hay que harcerlo ya* ‘It must be done already’). We also discuss the approach of Curco&Erdely (2016). <br/> <br/> **Todavía, aún.** We argue that these adverbials are not synonymous.  *Todavía* is scalar like *still*, while *aún* is additive. Additivity permits the concessive interpretation that is also available for *aunque* (cf. additive particles in Hindi concessives)."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2019-11-01T11:00:00"
date_end: "2019-11-01T11:30:00"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- Aniko Csirmaz
- Daniel A. Razo
- admin

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

