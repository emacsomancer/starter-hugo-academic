---
title: 'Underlying structure of a class of adverbials'

event: 'The 92nd Annual Meeting of the Linguistic Society of America'
event_url: https://www.linguisticsociety.org/node/8208/schedule

location: Grand America Hotel, Salt Lake City, Utah

summary: "An optimality-theoretical analysis of a neologistic morphological process in Dread Talk."

abstract: "We examine the internal structure of a subclass of adverbials including several temporal adverbs, focussing on Hungarian, Hindi, and Nepali, with comparison to German and English. Connections between adverbials like *again* and *still* in Hindi, Nepali, and Hungarian suggest an underlying generalised relational adverbial, for which we present a templatic formalisation. This follows in the tradition of research which seeks to unite the different meanings of English *still* (e.g. Michaelis 1993, Beck 2016); we extend this to include *again* and *then*, and also examines the interaction of additive particles in the creation of 'concessive *still*'."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2018-01-06T10:30:00"
date_end: "2018-01-06T12:00:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Aniko Csirmaz

# Author notes (optional)
author_notes:
- "Equal contribution"
- "Equal contribution"

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

