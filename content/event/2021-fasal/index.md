---
title: "A compositional account of the apparent polysemy of Hindi ‘bhī’"

event: '11th meeting of the (Formal) Approaches to South Asian Languages [(F)ASAL-11]'
event_url: "https://sites.google.com/umn.edu/fasal11/home"

location: University of Minnesota

summary: "A continuation-semantics account of the polysemy of Hindi *bhī*, with preliminary phonetic analysis."

abstract: "Hindi possesses a number of focus-associated particles, including the (scalar-)additive particle *bhī*, which appears ambiguous between a plain additive and a scalar-additive reading. The scalar-additive reading often seems to involve a different prosody (differences in the realisation of the F0 excursion/low-star-high pitch accent, particularly the word-final F0 contour) on the focussed constituent. Rather than positing two homophonous focus-sensitive particles (one with an additional prosodic requirement), we analyse the special prosody as itself contributing a scalar ordering which composes with meaning of *bhī* using a continuation semantics analysis.</br></br>Preliminary phonetic analysis of Hindi shows clear indications of significant differences in the F0 contours of focussed element, particularly the Hp, i.e. the phrasal boundary tone, associated with scalar-additive *bhī* versus those associated with plain additive *bhī*, consistent with an analysis of a prosodic component with its own (compositional) semantic contribution. Further investigations will be carried out with a larger dataset to verify the details of the prosodic differences in the focus associates, including examining post-focal pitch-compression."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-03-28T00:00:00Z"
date_end: "2021-03-28T00:30:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Vandana Puri
- Archna Bhatia
- Aniko Csirmaz
tags: []

# Is this a featured talk? (true/false)
featured: true

url_code: ""
url_pdf: ""
url_slides: "slides/slade-fasal11.pdf"
url_video: "videos/slade-fasal11-bhi-recording.mp4"

---

