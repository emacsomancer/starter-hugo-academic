---
title: 'Overstanding Idren: Special Features of Rastafari English Morphology'

event: '2nd Rastafari Studies Conference and General Assembly – Rastafari, Coral Gardens and African Redemption: Issues, Challenges and Opportunities – Commemorating the 50th Anniversary of the Coral Gardens Massacre'
event_url: https://web.archive.org/web/20180117050218/http://ocs.mona.uwi.edu/index.php/rsc/2013

location: University of the West Indies, Mona, Jamaica

summary: "An investigation of two morphological processes in Dread Talk."

abstract: "This talk investigates the structure and development of two unique morphological (word-building) processes found in Rastafari Talk: 'overstandings', e.g. forms like *outformer*, *livicate* etc.; and 'I-words' like *ital*, *Issembly*, *inity* etc.</br></br>I show that these two RE morphological phenomena are distinct from any processes previously identified in other languages, both in terms of form and function. Overstandings like *downpress* (< *oppress*, treated as if composed of *up* + *press*) superficially resemble folk etymology, in which a morphologically-opaque word is re-analysed; e.g.*woodchuck* (< Algonquian *otchek*), *shamefaced* (< earlier English *shamefast* “stuck in shame”). The process of folk etymology, however, involves a misunderstanding of the original sense triggering a reformation, while overstandings involve an **intentional** re-etymologising where the change in form reflects change in meaning and/or perspective. That is, the standard English is treated as representing a lower, incomplete <u>under</u>standing, whereas the new RE form signals a higher comprehension (i.e. <u>over</u>standing). Many overstandings are formed on the basis of binary oppositions like *up* vs.*down*, *over* vs.*under*, *dead* vs. *(a)live*, *love* vs.*hate* etc. (reflecting an ideological binarity lexicalised by *Zion* vs.*Babylon*).</br></br>I-words involve a transformational process replacing initial syllables by the diphthong /ai/, thus *Iration* for *creation*. The underlying process involved seems historically connected to the restructuring of the RE pronominal system, which not only eliminates *me* in favour of *I* for all grammatical case forms, but also largely eschews the use of the second person form *you* (often replaced by *the I*). Not only is “you” replaced, but also other phonological sequences *yu*, thus *I-man* (/aiman/) from *human* (Jamaican Creole /juman/), and similarly forms like *inity*, *iniverse*, and that from this point the process was eventually generalised to include the replacement of any initial syllable by /ai/."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2013-08-12T00:00:00Z"
date_end: "2013-08-16T00:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: true

url_code: ""
url_pdf: ""
url_slides: "slides/slade-overstanding-slides.pdf"
url_video: ""

---

