---
title: "Hindi V+V collocations: the larger context and speculations on L2 acquisition issues"

event: 'International Conference on Hindi Grammar and Lexicon'
event_url: https://hindigramlex.sciencesconf.org/

location: INALCO, Paris

summary: An examination of the properties of Hindi verb-verb collocations, with a view on acquisition.
abstract: "Hindi V+V compounds, particularly the frequently occurring type exemplified by *khā liyā*, involving a 'main verb' in an apparent bare stem form, and another 'light verb' (a so-called 'vector verb'), are a central part of idiomatic fluent Hindi speech. However, the rules governing not only possible combinations but also how the use of a V+V collocation affects other aspects of the clause, as well as the semantic contribution of the vector, are all complex and involve idiosyncratic aspects; and thus are one area which poses a challenge to second language learners of Hindi.</br></br>Idiomatic command over the Hindi V+V subsystem is challenging, of course, for speakers of non-Indo-Aryan languages; however, even though similar V+V systems exist in most other Indo-Aryan languages, the details of the grammar of the system, and the particular allowed combinations (and their resulting meaning contribution) varies quite a bit between Indo-Aryan languages, and there is no simple function of lexical substitution that speakers of other Indo-Aryan languages could just to map directly from their language to Hindi. Further, Hindi uses a seemingly wider range of V+V combinations than many other Indo-Aryan languages, and the frequency of the occurrence of such combinations appears somewhat higher in Hindi than in closely-related languages.</br></br>For example, using a V+V combination for expressing a past tense of *mar* 'die' is frequent and idiomatic in Hindi: *vah mar gayā*, while the simplex counterpart *vah marā* is more limited in its distribution. In contrast, Nepali, which like other Indo-Aryan languages does employ V+V combinations, there is no possibility of using a V+V for the Nepali counterpart of Hindi *vah mar gayā*, only the simplex *u maryo* (~ Hindi *vah marā*).</br></br>Further, the grammatical aspects of Indo-Aryan V+V systems varies between languages. While in Hindi, in the case of a transitivity mismatch between the members of the V+V collocation, it is generally the transitivity of the light verb member which determines whether the entire collocation is treated as transitive or not for purposes of assigning ergative case; in Nepali, it is always the 'main', non-grammaticalised member of the V+V whose transitivity is relevant for ergative assignment. Thus Hindi *vah khānā khā gayā* ('he ate up the food', with no ergative marking on the subject; main verb *khā* 'eat', transitive; light verb *jā-* 'go', intransitive) but Nepali *us-le yo kām gariāeko cha* ('He has continued to do this work; with ergative marking (*-le*) on the subject; main verb *gar* 'do', transitive; light verb *āu-* 'come', intransitive).</br></br>The challenges faced by a second language learner aiming to acquire an idiomatic command over Hindi V+V collocations is similar in some ways to the challenges that second language learners of English face for learning the idiomatic use of verbal particles/phrasal verbs, like *eat up*, *wolf down*, *put out*, *set up*, etc. These too are central in English, and highly idiosyncratic, and the semantic effects of verbal particles involve a mixture of completion and attitudinal components.</br></br>Curiously, perhaps, the pragmatic/semantic contributions of the vector verb in Hindi V+V combinations are often rendered into idiomatic English by use of verbal particles. Thus *usne khānā khā liyā* is most felicitously rendered into English as 'he ate **up** the food'.</br></br> Thus, both Hindi V+V combinations and English verbal particle constructions involve a heterogeneous collection of semantic contributions, from completion to speaker attitude, even when it the same vector verb or particle involved. For instance, Hindi *le* 'take' can be used to indicate completion, as in *usne khānā khā liyā*, or used to signal that the speaker believes that action is agent-benefitting, as in *kah lo!* '(go on,) speak (for your own benefit)'.</br></br> Historically, V+V collocations of this type derived from constructions involving 'absolutive' forms of verbs (which themselves persist in Indo-Aryan languages as well, e.g., the Hindi *V+ke*, *V+kar* type), where another verbal form in the clause has been re-analysed as a 'light verb', serving primarily as a semantic modifier of the absolutive verb (which behaves then, semantically, as the main verb of the clause). While there are a few early examples of such constructions in Sanskrit which potentially show some degree of grammaticalisation, the modern Indo-Aryan V+V subsystem is not clearly attested until the modern era (at least for mainland Indo-Aryan languages; Sinhala attests early use of V+V constructions). And V+V collocations manifest in rather different forms in different Indo-Aryan languages, with differences in both inventory of vector verbs and differences in grammatical properties (as above).</br></br>In this talk, I present a brief overview of the history of V+V constructions in Indo-Aryan, and discussion variations, both in inventory, and in grammatical behaviour, in the V+V systems of Hindi in comparison to other Indian languages. I follow with some brief discussion of L2 teaching methods for English particle verb constructions and discuss to what extent some of these may be adapted to L2 practices to facilitate L2 acquisition of the Hindi V+V subsystem."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-11-03T00:00:00Z"
date_end: "2021-11-04T00:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: true

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

