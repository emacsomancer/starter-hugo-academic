---
title: The semantics of Jamaican Creole verbal reduplication

event: 'Society for Caribbean Linguistics - 21st Biennial Conference'
event_url: http://www.scl-online.net/Conferences/2016/

location: University of the West Indies, Mona, Jamaica

summary: A formal analysis of the semantics of Jamaican Creole reduplicated verbal expressions.
abstract: "This paper pursues a formal semantic analysis of Jamaican Creole [JC] verbal reduplication, with comparison to superficially similar constructions in standard English [SE], focussing on reduplication involving pluractionality. Alongside of other reduplicative formations, JC uses two productive verbal reduplications, one contributing an intensive sense, the other an iterative sense (Gooden 2003; Gooden, Kouwenberg, LaCharité 2003 et seq.). <br/><br/>**Intensive:** In JC, both intensive and iterative reduplication involve reduplication of the full verb, but in the case of a complex verb+particle base, the intensive reduplication only doubles the verb, whereas iterative reduplication copies both verb and particle, e.g., /ǌam-ǌam-aaf/ “to eat completely (intensive)” vs. /bʊod-op-bʊod-op/ “to repeatedly seal with boards (iterative)” (Gooden 2003). Further, in the case of a simplex base, intensive reduplication (but not iterative) involve a high pitch on reduplicant: /blʊ́o-blʊo/ “to blow hard (intensive)” (Gooden 2003). The JC intensive reduplication resembles in some respects contrastive focus reduplication in SE (Ghomeshi et al. 2004), e.g. *it’s tuna salad, not sálad-salad*, both in terms of prosody (high pitch on the reduplicant) and also in that the latter reduplication often fails to copy all morphemes associated with the verb, e.g., *here are the glóve-gloves*. The semantic components of JC intensive reduplication and SE contrastive reduplication are similar but not identical, as the latter is not a true intensive per se but rather usually means something like “real” or “true”, being opposed to some contextually-determined contrast set. <br/> <br/>**Iterative:** The JC iterative reduplication is ambiguous between continuous action, and discontinuous repeated action, e.g., /luk-luk/ generally receives a continuous reading “keep looking” but can also be interpreted discontinuously as “look repeatedly” (Kouwenberg & LaCharité 20O1). This formation is superficially similar to SE looked and looked and looked again and again; the former type receiving a continuous reading and the latter a discontinuous reading. <br/><br/>Adopting a Lasersohn-style (1995) analysis of pluractionality (assuming that both the JC and the SE constructions involve an abstract pluractional morpheme which overtly manifests as reduplication, either at the morphological or syntactic level; cp. Beck & von Stechow 2007), the basic meaning of the JC iterative morpheme is:<br/> <br/> (1) V-PA(X) ⇔ ∀𝑒 ∈ 𝑋[𝑃 (𝑒)] ∧ |𝑋| ≥ 𝑛 <br/> <br/> That is, for some verbal predicate P, P is made up of a number of subevents, all of which are also instances of P, and the cardinality of these subevents is equal to or greater than some value n (fixed by a combination of lexical and pragmatic factors). The necessarily discontinuous readings can be formalised as: <br/><br/>(2) V-PA(X) ⇔ ∀𝑒, 𝑒′ ∈ 𝑋[𝑃 (𝑒) ∧ [¬𝜏(𝑒) ∘ 𝜏(𝑒′ ) ∧ ∃𝑡[(𝜏(𝑒) ≺ 𝑡 ≺ 𝜏(𝑒′ )∨ <br/>        (𝑒′ ) ≺ 𝑡 ≺ 𝜏(𝑒)) ∧ ¬𝑒″ (𝑉 (𝑒″ ) ∧ 𝑡 = 𝜏(𝑒″ )]] ∧ |𝑋| ≥ 𝑛 <br/><br/> In words, as in the paraphrase of (1) with an added between-ness condition requiring that between the run-time of every two non-overlapping subevents which are instances of P there be a time at which occurs no event which is an instance of P. To capture necessarily continuous readings the between-ness condition is negated. Towards a general formal analysis of JC reduplication: The above analysis can be generalised to cover other productive JC reduplication, such as /blakɪ-blakɪ/ “having black spots” and /bwaɪɪ-bwaɪɪ/ “characteristically boyish” by changing quantification over subevents to quantification over sub-parts more generally."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2016-08-02T18:30:00Z"
date_end: "2016-08-02T19:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: "slides/slade-creole-redup.pdf"
url_video: ""

---

