---
title: 'An Optimality-Theoretical Analysis of a Novel Morphological Process in Rasta Talk'

event: 'The 92nd Annual Meeting of the Linguistic Society of America'
event_url: https://www.linguisticsociety.org/node/8208/schedule

location: Grand America Hotel, Salt Lake City, Utah

summary: "An optimality-theoretical analysis of a neologistic morphological process in Dread Talk."

abstract: "The speech of members of the Rastafari community (originating in Jamaica) exhibits various linguistic innovations, including garden-variety extensions of productive morphological patterns to produce neologisms like upful “positive” or livity “lifestyle”, as well as examples of punning/word-playlike politricks “politics”. This speech variety, often called Rasta Talk also exhibits examples of more unusual linguistic innovations, known as ‘I-words’, such as Iration “creation” and Yood “food”, both part of larger systems of morphological transformations. I present an Optimality Theoretic treatment which handles apparent exceptional patterns in I-word creation."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2018-01-05T10:30:00"
date_end: "2018-01-05T12:00:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

