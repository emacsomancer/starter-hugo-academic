---
title: "An approach to the apparent polysemy of the Hindi (scalar-)additive particle ‘bhī’"

event: '39th meeting of the West Coast Conference on Formal Linguistics [WCCFL39]'
event_url: "https://sites.google.com/view/wccfl2021/"

location: University of Arizona, Tucson

summary: "A continuation-semantics account of the polysemy of Hindi *bhī*, with preliminary phonetic analysis."

abstract: "Hindi possesses a number of focus-associated particles, including the (scalar-)additive particle *bhī*, which appears ambiguous between a plain additive and a scalar-additive reading. The scalar-additive reading often seems to involve a different prosody (differences in the realisation of the F0 excursion/low-star-high pitch accent, particularly the word-final F0 contour) on the focussed constituent. Rather than positing two homophonous focus-sensitive particles (one with an additional prosodic requirement), we analyse the special prosody as itself contributing a scalar ordering which composes with meaning of *bhī* using a continuation semantics analysis.</br></br>Preliminary phonetic analysis of Hindi shows clear indications of significant differences in the F0 contours of focussed element, particularly the Hp, i.e. the phrasal boundary tone, associated with scalar-additive *bhī* versus those associated with plain additive *bhī*, consistent with an analysis of a prosodic component with its own (compositional) semantic contribution. Further investigations will be carried out with a larger dataset to verify the details of the prosodic differences in the focus associates, including examining post-focal pitch-compression."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-04-11T00:00:00Z"
date_end: "2021-04-11T00:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Archna Bhatia
- Vandana Puri
- Aniko Csirmaz
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: "slides/Slade_223-wccfl39.pdf"
url_video: ""

---

