---
title: 'South Asian relative-correlatives and unexpected particles'

event: '2017 Annual Conference on South Asia: Special Session on Perspectives on South Asian Syntax: A Panel in Honor of Alice Davison and James W. Gair'
event_url: https://confsouthasia.wiscweb.wisc.edu/wp-content/uploads/sites/48/2017/11/2017-Conference-Program-Book.pdf

location: "The Madison Concourse Hotel & Governor's Club, Madison, Wisconsin"

summary: "A crosslinguistic examination of different varieties of quantifier-particles which appear in the formation of relative clauses, focussing on Sinhala & Dravidian in comparison to Nepali and Hindi."

abstract: "Crosslinguistically many languages use morphemes from two series of particles — split roughly into additive/conjunctive/universal morphemes (Sinhala *-t*, Dravidian *um*, Japanese *mo*) and interrogative/disjunctive/existential morphemes (Sinhala *da, Dravidian *ō*, Japanese *ka*) — in a wide range of 'quantificational' contexts. [Cf. Kratzer & Shimoyama 2002; Szabolcsi 2010, 2015; Slade 2011; Mitrović 2014.] Relative-correlative constructions [RCC] in Dravidian and literary/classical varieties of Sinhala (as well as in Mangalore Saraswat Konkani and Dakkhini Urdu, cf. Hock 2016) use a quantifier particle as a 'closing particle' in the relative clause. Unexpectedly, the quantifier particle involved in these languages is part of the disjunctive/existential group, as opposed to the case in Nepali, where a member of the additive/universal group appears (as also in Burushaski, Basque, cf. Hock 2005). Thus the use of the particle da in classical/literary Sinhala relative clauses (which often have a free-choice flavour) like (1), Nepali *pani* in free choice constructions like (2) (cp. also Hindi *bhī*, cf. Dayal 1996). Since RCCs often have a free-choice interpretation the presence of particles associated with universal quantification like Nepali *pani* &c. is not unexpected, but the existential-associated particles of Sinhala and Dravidian RCCs are. I provide an analysis which treats existentially-associated particles (Sinhala *da*, Tamil/Malayalam *ō*) as variables over choice-functions carrying an anti-singleton presupposition (cf. Alonso-Ovalle & Menéndez-Benito 2010), which accounts for their use in the formation of epistemic indefinites, and can be extended to explain their use in the formation of relative clauses."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2017-10-28T10:30:00"
date_end: "2017-10-28T12:15:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

