---
title: 'The pieces of Indo-Aryan aspectual adverbs'

event: 'Formal Approaches to South Asian Languages [FASAL] 7'
event_url: http://fasal.mit.edu

location: MIT, Cambridge, Massachusetts

summary: "An analysis of aspectual adverbials in Hindi & Nepali which provides an account of the meaning which captures the inter-relations signalled by their compositional morphology."

abstract: "We examine a subset of aspectual adverbs in Hindi & Nepali and provide an account of their meaning which captures the inter-relations signalled by their compositional morphology. Specifically we examine Hindi *phir* (ambiguously 'then, after that' or 'again') and *phir bhī* ('still',both temporal and concessive), as well as their Nepali counterparts *pheri* and *pheri pani*. Though in languages like English, *still*, *then*, *again* have no obvious connection, in Hindi & Nepali they do, and thus ideally the morphological compositionality of these items can be related to a (at least partially) compositional semantics. Likewise, items like English *then* have a variety of interpretations which are not always available to their crosslinguistic counterparts. Such considerations drive this investigation of possible decompositions of aspectual adverbs into more basic pieces."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2017-03-05T12:15:00"
date_end: "2017-03-05T14:00:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
- Aniko Csirmaz

tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

---

