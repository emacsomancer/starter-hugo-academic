---
title: 'Why are there disjunctive particles in Sinhala & Dravidian relative-correlatives?: existential particles in nonexistential environments'

event: '23rd International Conference on Historical Linguistics: Workshop on Logical Vocabulary & Logical Change'

event_url: https://mitrovic.co/logvoc/

location: "Hotel Contessa, San Antonio, Texas"

summary: "An analysis of the role of disjunctively-associated quantifier particles in Sinhala and Dravidian relative clauses."

abstract: "Relative-correlative constructions in Dravidian and literary varieties of Sinhala use a quantifier particle as a 'closing particle' in the relative clause; such constructions often involve free choice interpretations. Unexpectedly, the quantifier particle involved is part of the disjunctive/existential group (used in forming disjunctions, indefinites, questions) rather than the additive/universal group, as opposed to the case in Hindi and other languages. I provide an analysis which treats these particles as variables over choice-functions carrying an anti-singleton presupposition, which accounts for their use in the formation of epistemic indefinites in Sinhala & Malayalam. In the case where such particles are internal to a relative clause, the quantificational force contributed by the relative pronoun produces a universally quantified environment out-scoping the existentially-bound choice function variables. This forms part of a larger effort to understand the nature of particles expressing what seem to be elementary logical operations (Szabolcsi, Slade, Mitrović &c.)"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2017-08-03T14:30:00"
date_end: "2017-08-03T14:55:00"
all_day: false

# Schedule page publish date (NOT talk date).
# publishDate: "2017-01-01T00:00:00Z"

authors: 
- admin
tags: []

# Is this a featured talk? (true/false)
featured: false

url_code: ""
url_pdf: ""
url_slides: "https://mitrovic.co/papers/slade-sinhala-relative-correlatives-particles.pdf"
url_video: "https://www.youtube.com/watch?v=GrHv7KETxvU&feature=youtu.be&t=4s"

---

