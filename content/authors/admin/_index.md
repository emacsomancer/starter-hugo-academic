---
# Display name
title: Benjamin Slade

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Associate Professor of Linguistics

# Organizations/Affiliations to show in About widget
organizations:
- name: University of Utah
  url: https://linguistics.utah.edu/

# Short bio (displayed in user profile at end of posts)
bio: My research interests include formal semantics and syntax, historical linguistics, South Asian and Caribbean languages, and the use of computational concepts in formal linguistics.

# Interests to show in About widget
interests:
- Formal Semantics 
- Historical Linguistics
- Formal Syntax & Morphology
- Computational Approach to Formal Linguistics


# Education to show in About widget
education:
  courses:
  - course: PhD in Linguistics
    institution: University of Illinois at Urbana/Champaign
    year: 2011
  - course: MA in Linguistics
    institution: University of Illinois at Urbana/Champaign
    year: 2008
  - course: MA in Cognitive Science
    institution: Johns Hopkins University
    year: 2004
  - course: BA in English
    institution: Johns Hopkins University
    year: 1999 

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope-open
  icon_pack: fas
  link: mailto:b.slade@utah.edu
- icon: envelope
  icon_pack: fas
  link: mailto:slade@lambda-y.net
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=N5H19_AAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/emacsomancer
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/users/emacsomancer/projects

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true
---

My research focuses on formal and historical aspects of linguistics, especially formal semantics & syntax. My areal interests include South Asia (Sanskrit, Hindi, Sinhala, Nepali, Malayalam etc) and the Caribbean (Jamaican Patois, Dread Talk), as well as early Indo-European (especially Indic & Germanic). 

My current research projects include investigations of the syntax & semantics of quantifier particles; the inner semantics of aspectual adverbials; a machine-learning approach to automated focus identification & labelling. 

My past work includes studies of epistemic indefinites; the morphosyntax and semantics of verb-verb collocations in South Asian languages; the rise and spread of morphological & orthographic innovations in the cyberpunk subculture; morphological processes in Rastafari Dread Talk. 

{{< icon name="download" pack="fas" >}} Download my {{< staticref "https://faculty.utah.edu/bytes/curriculumVitae.hml?id=u0908006" "newtab" >}}CV{{< /staticref >}}.


**Video introduction to some of my research:**

<video id="Benjamin-Slade-linguist" poster="/home/slade-website_video.obs.placeholder.jpg" controls>
<!-- <source src="/home/slade-website_video.obs.vp9.webm" type='video/webm; codecs="vp9, opus"' /> -->
<source src="/home/slade-website_video.obs.webm" type='video/webm; codecs="vp8, vorbis"' />
<source src="/home/slade-website_video.obs.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
</video>

